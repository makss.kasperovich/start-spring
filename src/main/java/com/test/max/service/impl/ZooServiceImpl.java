package com.test.max.service.impl;

import com.test.max.dto.Zoo;
import com.test.max.service.ZooService;
import org.springframework.stereotype.Service;

@Service
public class ZooServiceImpl implements ZooService {

    @Override
    public Zoo createZoo(String zooName) {
        Zoo zoo = new Zoo();
        zoo.setName(zooName);
        return zoo;
    }
}
