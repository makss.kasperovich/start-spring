package com.test.max.service;

import com.test.max.dto.Zoo;

public interface ZooService {

    Zoo createZoo(String zooName);

}
