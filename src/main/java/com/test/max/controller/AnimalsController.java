package com.test.max.controller;

import com.test.max.dto.Animal;
import com.test.max.dto.Dog;
import com.test.max.dto.Zoo;
import com.test.max.service.ZooService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/zoo")
public class AnimalsController {

    private final ZooService zooService;

    @Autowired
    public AnimalsController(ZooService zooService) {
        this.zooService = zooService;
    }

    @RequestMapping("/all")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @RequestMapping(value = "/createZoo", method = RequestMethod.GET)
    public Zoo createZoo() {
        return zooService.createZoo("Minsk Zoo");
    }

    @RequestMapping(value = "/createAnimals", method = RequestMethod.GET)
    public List<Animal> createDogs() {
        Dog dog1 = new Dog("Shyarik", 2);
        Dog dog2 = new Dog("Tuzik", 10);
        List<Animal> animals = new ArrayList<>();
        animals.add(dog1);
        animals.add(dog2);
        return animals;
    }

    @RequestMapping(value = "/createZoo/{name}", method = RequestMethod.POST)
    @ResponseBody
    public Zoo postMyData(@PathVariable String name,
                          @RequestBody List<Animal> animals) {
        Zoo zoo = zooService.createZoo(name);
        if(animals != null){
            zoo.setAnimalsList(animals);
        }
        return zoo;
    }

}
